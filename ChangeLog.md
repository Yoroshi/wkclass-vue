# Changelog

## Alpha 1.5.0
---
+ Tweak: (Large) Settings page design redone
+ Addition: Users can now upload their own images for their banner and avatars
+ Tweak: Back button added to settings page 
+ Fix: The review counter will now count correctly
+ Tweak: Per fix above the counter counts reviews now and after separatly

## Alpha 1.4.2_pass
---
+ Tweak: begin redesigning the settings page

## Alpha 1.4.1
---
+ Addition: Subscription (real time update) status is now visualized
+ Tweak: More overviewData update error visualization
+ Tweak: Feedback page condensed
+ Addition: Feedback page now has issues listed (and link to real page for details)

## Alpha 1.4.0
---
+ Addition: Feedback page
+ Addition: Basic Issue Tracker and sender
+ Fix: User Update no longer breaks if user does not have a bio, it fills it automatically
+ Tweak: Validation is a lot more understandable on the users settings page

## Alpha 1.3.3
---
+ Additon: The welcome screen will automatically redirect users that already have an account to their page
+ Tweak: Better Resizing for smaller screens on users page
+ Tweak: New users will automatically be added to the original class for now
+ Addition: Visual feedback when the overviewData should not be updated before 20 min

## Alpha 1.3.2_pass
---
+ Tweak: OverviewData for the user pages will be loaded even if the overview page has not been initialized (One time)

## Alpha 1.3.1_pass
---
+ Addition: dynamic listeners for classes on overview screen

## Alpha 1.3_pass
---
+ Fix: Proper calculation of progress bar for vocab in UserPage
+ Addition: Title now updates on where the user is in the application instead of being a static title
+ Change: Update Summary Button is now a small fab
+ Change: The button to access the user settings is much more visible (FAB)
+ Addition: If user visits their own page they have the option to complete their reviews and lessons by an accompanying button
+ Addition: Toggleable Live updates for overviewInfo
+ Addition: User can now update their overview from their user page (FAB)
+ Adittion: Rudimentary classes system added

-----------------------------
Start of recording of changes