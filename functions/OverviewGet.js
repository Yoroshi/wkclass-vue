const moment = require('moment')
const axios = require('axios')
/* eslint-disable promise/no-nesting */
exports.handler = (apiKey, docRef, fs) => {
  const profileRef = fs.collection('profiles').doc(docRef).collection('basic')
  const instance = this.wkapi(apiKey)
  let basicInfoArray = []
  let level
  instance.get('/user').then(response => {
    basicInfoArray.push(moment().toISOString())
    basicInfoArray.push(response.data.data.level)
    level = response.data.data.level

    let assignments

    function getAssignment () {
      return instance.get('/assignments?levels=' + level).then(response => { assignments = response.data.data; return })
    }
    axios.all([getAssignment()]).then(axios.spread(() => {
      const overviewProgressInfo = this.overviewProgressNumbers(assignments, level)
      basicInfoArray.push(overviewProgressInfo)
      console.log(basicInfoArray)
      this.setBasicAPI(basicInfoArray, docRef)
      return
    })).catch()
    return
  }).catch()
}

const wkapi = (apiKey) => axios.create({
  baseURL: 'https://api.wanikani.com/v2/',
  method: 'get',
  timeout: 10000,
  headers: {'Authorization': 'Bearer ' + apiKey}
})

const getLevelNumbers = (level) => {
  const itemCount = [ [0, 0, 0], // Level 0
    [26, 18, 42], [34, 38, 89], [22, 33, 72], [33, 38, 105], [27, 42, 126], // Levels 1-5
    [20, 40, 118], [17, 33, 101], [15, 32, 136], [16, 35, 119], [15, 35, 117], // Levels 6-10
    [14, 38, 127], [11, 37, 129], [16, 37, 115], [6, 32, 116], [7, 33, 100], // Levels 11-15
    [6, 35, 118], [7, 33, 124], [8, 29, 135], [9, 34, 106], [6, 32, 114], // Levels 16-20
    [7, 32, 108], [6, 31, 116], [7, 31, 99], [7, 31, 121], [8, 33, 101], // Levels 21-25
    [3, 33, 125], [8, 32, 107], [5, 34, 118], [8, 33, 108], [6, 31, 103], // Levels 26-30
    [7, 36, 113], [7, 33, 112], [5, 32, 103], [5, 34, 125], [4, 32, 101], // Levels 31-35
    [7, 33, 98], [4, 32, 113], [6, 32, 101], [8, 34, 101], [3, 32, 116], // Levels 36-40
    [4, 30, 109], [5, 33, 98], [5, 35, 110], [5, 34, 106], [3, 35, 98], // Levels 41-45
    [2, 37, 104], [3, 36, 94], [3, 37, 105], [2, 33, 104], [3, 35, 88], // Levels 46-50
    [1, 35, 77], [0, 35, 98], [2, 35, 104], [0, 35, 115], [0, 35, 76], // Levels 51-55
    [0, 35, 88], [2, 35, 93], [0, 35, 81], [1, 35, 79], [1, 32, 65] // Levels 56-60
  ]
  return {radical: itemCount[level][0], kanji: itemCount[level][1], vocab: itemCount[level][2]}
}

const setBasicAPI = (data, documentRef) => {
  const formatedObject = { timestamp: data[0], level: data[1], overviewInfo: data[2] }
  fs.collection('profiles').doc(documentRef).collection('basic').doc().set(formatedObject)
}
const overviewProgressNumbers = (assignments, level) => {
  let kanjiPassCount = 0
  let radicalPassCount = 0
  let vocabPassCount = 0
  for (let assignment of assignments) {
    if (assignment.data.subject_type === 'kanji' && assignment.data.passed === true) {
      kanjiPassCount += 1
    }
    if (assignment.data.subject_type === 'radical' && assignment.data.passed === true) {
      radicalPassCount += 1
    }
    if (assignment.data.subject_type === 'vocabulary' && assignment.data.passed === true) {
      vocabPassCount += 1
    }
  }
  const levelNumbers = this.getLevelNumbers(level)
  const progressNumbers = { radicalPassed: radicalPassCount, kanjiPassed: kanjiPassCount, vocabPassed: vocabPassCount, radicalCount: levelNumbers.radical, kanjiCount: levelNumbers.kanji, vocabCount: levelNumbers.vocab }
  return progressNumbers
}