const axios = require('axios')
/* eslint-disable promise/no-nesting */
exports.handler = (apiKey, docRef, fs) => {
    let wkapi = (apiKey) => axios.create({
        baseURL: 'https://api.wanikani.com/v2/',
        method: 'get',
        timeout: 60000,
        headers: {'Authorization': 'Bearer ' + apiKey}
    })
      
    const setSummary = (data, documentRef) => {
        const formatedObject = { summary: data }
        fs.collection('profiles').doc(documentRef).update(formatedObject)
    
    }
  const instance = wkapi(apiKey)
  instance.get('/summary').then(response => { 
      let reviewCount = 0
      let lessonCount = 0
      response.data.data.reviews.forEach(reviewHour => {
          reviewCount += reviewHour.subject_ids.length
      })
      lessonCount = response.data.data.lessons[0].subject_ids.length
    summaryObject = {
      reviews: reviewCount,
      lessons: lessonCount
    }
    setSummary(summaryObject, docRef)
    return 
  }).catch(error => {
      console.log(error)
  })
}
