const moment = require('moment')
const axois = require('axios')
/* eslint-disable promise/no-nesting */
exports.handler = (apiKey, docRef, fs) => {
  const instance = this.wkapi(apiKey)

  // For Each Public User
    let assignments = []
    let count

    function getAssignment () {
      return instance.get('/assignments').then(response => { 
        assignments.push(response.data.data)
          if (response.pages.next_url) {
            let next_url = response.pages.next_url.replace('https://api.wanikani.com/v2/', '')
            getAssignmentLoop(next_url)
          }
        return 
      })
    }
    function getAssignmentLoop (next_url) {
        count += 1
        instance.get(next_url).then(response => {
            assignments.push(response.data.data)
            if (response.pages.next_url) {
                let succ_url = response.pages.next_url.replace('https://api.wanikani.com/v2/', '')
                console.log('another Assignment Page was Gathered')
                getAssignmentLoop(succ_url)
            } else {
                console.log('no more Assignment Pages')
            }
            return
        }).catch()
    }
    axios.all([getAssignment()]).then(axios.spread(() => {
      // Now Sort though the data
      let userAssignmentData
      for (let assignments in assignmentPages) {
        for (let assignment in assignments) {
            const reducedObject = {
                subject_id: assignment.subject_id,
                subject_type: assignment.subject_type,
                level: assignment.level,
                srs_stage: assignment.srs_stage,
                started_at: assignment.started_at,
                passed_at: assignment.passed_at,
                avaliable_at: assignment.avaliable_at
            }
            userAssignmentData.push(reducedObject)
        }
      }
      console.log(userAssignmentData.length)
      this.setUserData(userAssignmentData, docRef, 'assignmentData')
      return
    })).catch()
}

const wkapi = (apiKey) => axios.create({
  baseURL: 'https://api.wanikani.com/v2/',
  method: 'get',
  timeout: 10000,
  headers: {'Authorization': 'Bearer ' + apiKey}
})

const setUserData = (data, documentRef, type) => {
  fs.collection(type).doc(documentRef).set(data)
}