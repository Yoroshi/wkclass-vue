'use strict';
const OverviewDeleteOF = require('./OverviewDeleteOF');
const OverviewGet = require('./OverviewGet');
const userDataUpdater = require('./UserDataUpdater');
const userSummaryUpdater = require('./userSummaryUpdater');

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();
const fs = admin.firestore();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   response.status(200).send("Hello from Firebase!");
// });

exports.OverviewDeleteOF = functions.https.onCall((data, context) => {
  OverviewDeleteOF.handler(data, context, fs);
});

exports.userDataUpdater = functions.https.onCall((data, context) => {
  fs.collection('profiles').where('assignmentData', '==', 'true').get().then(snapshot => {
    snapshot.forEach(doc => {
      userDataUpdater.handler(doc.data().apiKey, doc.id, fs);
    })
    return
}).catch()
});

exports.userSummaryUpdater = functions.https.onCall((data, context) => {
  // For Every User
  fs.collection('profiles').get().then(snapshot => {
    snapshot.forEach(doc => {
      const apiKey = doc.data().apiKey
      const docRef = doc.id
      // Get Summary
      userSummaryUpdater.handler(apiKey, docRef, fs)
    })
    return
  }).catch(error => {
      console.log(error)
  })
});

exports.userAddListener = functions.firestore.document('profiles/{documentUid}').onCreate((event, context) => {
  const user = event.data().username
  let dataToAdd = {}
  dataToAdd[context.params.documentUid] = user
  fs.collection('profiles').doc('userstats').update(dataToAdd)
  console.log('New User Created Adding to the Users List')
  OverviewGet.handler(event.data().apiKey, context.params.documentUid, fs)
  console.log('Ran User Overview Init')
  return 'User Added'
})

exports.userDeleteListener = functions.firestore.document('profiles/{documentUid}').onDelete((event, context) => {
  const user = event.data().usernames
  let dataToAdd = user
  delete dataToAdd[context.params.documentUid]
  fs.collection('profiles').doc('userstats').update(dataToAdd)
  console.log('Ran User Deletion')
  return 'User Deleted'
})

exports.userUpdateListener = functions.firestore.document('profiles/{documentUid}').onUpdate((change, context) => {
  const before = change.before.data()
  const after = change.after.data()
  if (before.username === after.username) {
    // Nothing has changed, do nothing
    console.log('Username did no update')
  } else {
    // Changed Data
    let dataToAdd = {}
    // dataToAdd[context.params.documentUid] = after.username
    fs.collection('profiles').doc('userstats').get().then(snapshot => {
      dataToAdd = snapshot.data().usernames
      dataToAdd[context.params.documentUid] = after.username
      fs.collection('profiles').doc('userstats').update({usernames: dataToAdd})
      return
    }).catch()
    console.log('Change In Username, Updated Userstats')
  }

  if (before.apiKey === after.apiKey) {
    console.log('APIKey was not altered')
  } else {
    console.log('APIKey was altered running reinit')
    OverviewGet.handler(after.apiKey, context.params.documentUid, fs)
  }
  return 'User Update Has Occured'
})
