exports.handler = (data, context, fs) => {
  const profileRef = fs.collection('profiles').doc(data.docRef).collection('basic')

  profileRef.get().then(snap => {
    if (snap.size >= 11) {
      const snapsize = snap.size - 10
      if (snapsize >= 0 ) {
        profileRef.orderBy('timestamp').limit(snapsize).get().then(docs => {
          docs.forEach(doc => {
            profileRef.doc(doc.id).delete().then(() => {
              console.log('Deleted Overflow Data From' + doc.id)
              return
            }).catch()
          })
          return
        }).catch()
      }
    }
    // res.send('OverviewDeleteOF Ran Sucessfully')
    return
  }).catch()
};