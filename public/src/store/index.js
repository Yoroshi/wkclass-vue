import Vuex from 'vuex'

import Vue from 'vue'
import { auth, fs } from '../components/Firebase.vue'

Vue.use(Vuex)

/* eslint-disable no-new */
export default new Vuex.Store({
  state: {
    loggedUser: {
      apiKey: '',
      bannerPhoto: '',
      currentUser: '',
      photo: '',
      refDoc: '',
      uid: '',
      loggedIn: false,
      username: '',
      googlePhoto: ''
    },
    OverviewData: [],
    overviewUsers: [],
    classes: {},
    subscriber: null,
    userPageUsers: [],
    usernames: [],
    progressBar: {
      Value: 0,
      active: false,
      updatingText: '',
      progressText: ''
    },
    subjects: null
  },

  getters: {
    userPageUserID: (state) => (wantedUser) => {
      if (state.userPageUsers.find(user => user.uid === wantedUser)) {
        return state.userPageUsers.find(user => user.uid === wantedUser)
      } else if (state.userPageUsers.find(user => user.ref === wantedUser)) {
        return state.userPageUsers.find(user => user.ref === wantedUser)
      } else {
        return {
          apiKey: '',
          bannerPhoto: '',
          currentUser: '',
          email: '',
          isPublic: false,
          loggedIn: false,
          photo: '',
          refDoc: '',
          uid: '',
          username: ''
        }
      }
    },
    userPageUser: (state) => (wantedUser) => {
      if (state.userPageUsers.find(user => user.username === wantedUser)) {
        return state.userPageUsers.find(user => user.username === wantedUser)
      } else {
        return {
          bannerPhoto: '',
          currentUser: '',
          photo: '',
          refDoc: '',
          uid: '',
          username: ''
        }
      }
    },
    usernameArray (state) {
      return Object.values(state.usernames)
    },
    getPageUserOverview: (state) => (wantedUser) => {
      if (state.classes) {
        if (wantedUser.length <= 25) {
          let classroomData = Object.values(state.classes)
          let usersClassRecord
          for (let classroom of classroomData) { usersClassRecord = Object.values(classroom).find(user => user.username === wantedUser) }
          return usersClassRecord
          // return Object.values(state.classes).find(user => user.username === wantedUser)
        } else {
          let classroomData = Object.values(state.classes)
          let usersClassRecord
          for (let classroom of classroomData) { usersClassRecord = Object.values(classroom).find(user => user.ref === wantedUser) }
          return usersClassRecord
          // return Object.values(state.classes).find(user => user.ref === wantedUser)
        }
      } else {
        // Dummy Data
        console.log('Did not get the overview')
        return null
      }
    },
    getClassOverviewData: (state) => (wantedClass) => {
      if (state.classes.hasOwnProperty(wantedClass)) {
        // If the class has already been populated to the store
        return state.classes[wantedClass]
      } else {
        // If not we gotta return loading
        return { loading: true }
      }
    },
    getLevelSubjects: (state) => (level) => {
      return {...state.subject.radicals[level], ...state.subject.kanji[level], ...state.subject.vocabulary[level]}
    }

  },

  actions: {
    pushLoggedUser ({commit}, user) {
      return new Promise((resolve, reject) => {
        commit('setLoggedUser', user)
        commit('addPageUser', user)
        resolve()
      })
    },

    fetchLoggedUser ({commit}) {
      return new Promise((resolve, reject) => {
        let loggedUser = {}
        auth.onAuthStateChanged((user) => {
          if (user) {
            // User is signed in.
            loggedUser.loggedIn = true
            loggedUser.currentUser = user.displayName
            loggedUser.uid = user.uid
            loggedUser.googlePhoto = user.photoURL
            fs.collection('profiles').where('ref', '==', user.uid).get().then(snapshot => {
              snapshot.forEach(doc => {
                if (doc.data().photo != null) { loggedUser.photo = doc.data().photo } else { loggedUser.photo = 'https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png' }
                if (doc.data().bannerPhoto != null) { loggedUser.bannerPhoto = doc.data().bannerPhoto } else { loggedUser.bannerPhoto = '' }
                if (doc.data().username != null) { loggedUser.username = doc.data().username } else { loggedUser.username = '' }
                if (doc.data().apiKey != null) { loggedUser.apiKey = doc.data().apiKey } else { loggedUser.apiKey = '' }
                if (doc.data().email != null) { loggedUser.email = doc.data().email } else { loggedUser.email = '' }
                if (doc.data().public != null) { loggedUser.isPublic = doc.data().public } else { loggedUser.isPublic = false }
                loggedUser.summary = doc.data().summary
                loggedUser.bio = doc.data().bio
                loggedUser.refDoc = doc.id
                loggedUser.classes = doc.data().classes
                commit('setLoggedUser', loggedUser)
                commit('addPageUser', loggedUser)
                console.log('Ran it from watcher!')
                resolve()
              })
            }).catch(error => {
              console.log(error)
              loggedUser.photo = 'https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png'
              loggedUser.bannerPhoto = ''
              loggedUser.username = ''
              loggedUser.apiKey = ''
              loggedUser.email = ''
              loggedUser.isPublic = false
              loggedUser.refDoc = ''
              loggedUser.summary = {lessons: 0, reviews: 0}
              loggedUser.bio = ''
              loggedUser.classes = []
              commit('setLoggedUser', loggedUser)
              commit('addPageUser', loggedUser)
              console.log('Ran it from watcher! (Failed to get from server so have this data instead!!)')
              resolve()
            })
          } else {
            loggedUser.photo = 'https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png'
          // console.log('Noone is logged in')
          }
        })
      })
    },

    fetchPageUser ({commit, state}, wantedUser) {
      return new Promise((resolve, reject) => {
        if (Object.values(state.usernames).find(user => user === wantedUser)) {
          // console.log('User is there')
          fs.collection('profiles').where('username', '==', wantedUser).get().then(snapshot => {
            let user = {}
            snapshot.forEach(doc => {
              if (snapshot != null) {
                user.uid = doc.data().ref
                user.isPublic = doc.data().public
                user.username = doc.data().username
                user.photo = doc.data().photo
                user.bannerPhoto = doc.data().bannerPhoto
                user.bio = doc.data().bio
                user.summary = doc.data().summary
                user.classes = doc.data().classes
                commit('addPageUser', user)
              }
            })
          }).catch(function (error) {
            console.log('Error getting documents: ', error)
          })
        } else {
          // console.log('User is not')
          resolve()
        }
      })
    },

    fetchUsers ({commit}) {
      fs.collection('profiles').doc('userstats').get().then(snapshot => {
        commit('setUserStatsUsers', snapshot.data().usernames)
      })
    },

    fetchOverviewUsers ({commit}) {
      // Get everything and put it in a store
      return new Promise((resolve, reject) => {
        fs.collection('profiles').where('public', '==', true).get().then(snapshot => {
          snapshot.forEach(doc => {
            // Set simple variable for accessing the overview users later
            commit('addOverviewUser', doc.id)
            fs.collection('profiles').doc(doc.id).collection('basic').get().then(() => {
              const username = doc.data().username
              const ref = doc.data().ref
              const photo = doc.data().photo
              fs.collection('profiles').doc(doc.id).collection('basic').orderBy('timestamp', 'desc').limit(1).get().then(snapshot => {
                snapshot.forEach(child => {
                  const dataToAdd = { timestamp: child.data().timestamp, ref: ref, username: username, level: child.data().level, photo: photo, overviewInfo: { ...child.data().overviewInfo } }
                  commit('addOverviewData', dataToAdd)
                })
              })
            })
          })
          resolve()
        })
      })
    },

    updateOverviewUsers ({commit, state}, data) {
      // Get Overview To Update
      const overviewIndex = state.OverviewData.findIndex(user => user.ref === data.ref)
      const oldOverviewData = state.OverviewData.find(user => user.ref === data.ref)
      // Get New Data
      const username = data.username
      const ref = data.ref
      const photo = oldOverviewData.photo
      fs.collection('profiles').doc(data.docRef).collection('basic').orderBy('timestamp', 'desc').limit(1).get().then(snapshot => {
        snapshot.forEach(child => {
          const dataToAdd = { timestamp: child.data().timestamp, ref: ref, username: username, level: child.data().level, photo: photo, overviewInfo: { ...child.data().overviewInfo } }
          // Call an remove
          commit('addOverviewData', dataToAdd)
          commit('removeOverviewData', overviewIndex)
        })
      })
    },
    watchClassData ({commit, state}, data) {
      return new Promise((resolve, reject) => {
        let classroom, hidden, refresh
        [classroom, hidden, refresh] = data
        let subscriber = state.subscriber
        const classroomRef = fs.collection('classrooms').doc(classroom)
        if (refresh) {
          if (state.subscriber) {
            state.subscriber()
          }

          let subscriberRef = classroomRef.onSnapshot(function (doc) {
            let classrooms = state.classes
            const newData = { [classroom]: doc.data() }
            const oldData = classrooms
            const dataToSet = { ...oldData, ...newData }
            console.log(dataToSet)
            commit('setClassData', dataToSet)
          })
          commit('setSubscriber', subscriberRef)
          resolve()
        } else {
          if (!subscriber) {
            let subscriberRef = classroomRef.onSnapshot(function (doc) {
              let classrooms = state.classes
              const newData = { [classroom]: doc.data() }
              const oldData = classrooms
              const dataToSet = { ...oldData, ...newData }
              console.log(dataToSet)
              commit('setClassData', dataToSet)
            })
            if (hidden) {
              // Remove listener
              // state.subscriber()
              commit('setSubscriber', null)
              resolve()
            } else {
              commit('setSubscriber', subscriberRef)
              resolve()
            // Is visable so dont get rid of it
            }
          }
          if (subscriber) {
            if (hidden) {
              // Remove listener
              state.subscriber()
              commit('setSubscriber', null)
              resolve()
            } else {
              commit('setSubscriber', null)
              resolve()
              // Is visable so dont get rid of it
            }
          }
        }
      })
    },

    getClassData ({commit, state}, classroom) {
      return new Promise((resolve, reject) => {
        const classroomRef = fs.collection('classrooms').doc(classroom)

        classroomRef.get().then(function (doc) {
          let classrooms = state.classes
          const newData = { [classroom]: doc.data() }
          const oldData = classrooms
          const dataToSet = { ...oldData, ...newData }
          console.log(dataToSet)
          commit('setClassData', dataToSet)
          resolve()
        })
      })
    },

    /* overviewWatcher ({commit, state}, data) {
      let overviewUsers = state.overviewUsers
      // Watcher function
      const overviewWatch = (user) => {
        fs.collection('profiles').doc(user).collection('basic').orderBy('timestamp', 'desc').limit(1).onSnapshot(snapshot => {
          snapshot.forEach(child => {

            const dataToAdd = { timestamp: child.data().timestamp, ref: ref, username: username, level: child.data().level, photo: photo, overviewInfo: { ...child.data().overviewInfo } }
            // Call an remove
            commit('addOverviewData', dataToAdd)
            commit('removeOverviewData', overviewIndex)
          })
        })
      }
      // If not hidden
      if (!data) {
        // Initialize the watcher with a function
        for (let user in overviewUsers) {
          overviewWatch(user)
        }
      } else {
        // Run the function again to remove it
      }
    }, */

    updateProgressBar ({commit, state}, data) {
      const prev = state.progressBar
      const next = data
      // Merge the Two
      let dataToSet = {...prev, ...next}
      commit('setProgressBar', dataToSet)
    },
    setSubjects ({commit}, data) {
      commit('setSubjects', data)
    }
  },

  mutations: {
    addOverviewData (state, overviewUsers) {
      state.OverviewData.push(overviewUsers)
    },
    removeOverviewData (state, index) {
      state.OverviewData.splice(index, 1)
    },
    setLoggedUser (state, loggedUser) {
      state.loggedUser = loggedUser
    },
    addPageUser (state, user) {
      state.userPageUsers.push(user)
    },
    setUserStatsUsers (state, usernames) {
      state.usernames = usernames
    },
    setProgressBar (state, update) {
      state.progressBar = update
    },
    addOverviewUser (state, docRef) {
      state.overviewUsers = docRef
    },
    setClassData (state, classData) {
      state.classes = classData
    },
    setSubscriber (state, subscriberRef) {
      state.subscriber = subscriberRef
    },
    setSubjects (state, subjects) {
      state.subjects = subjects
    }
  }
})
