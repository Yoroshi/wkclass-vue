import axios from 'axios'

export const wkapi = (apiKey) => axios.create({
  baseURL: 'https://api.wanikani.com/v2/',
  method: 'get',
  timeout: 60000,
  headers: {'Authorization': 'Bearer ' + apiKey}
})

export const axiosAll = axios.all
export const axiosSpread = axios.spread
