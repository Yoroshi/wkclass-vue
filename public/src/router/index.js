import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '@/components/Welcome'
import Overview from '@/components/Overview'
import User from '@/components/User'
import Support from '@/components/Support'
import Help from '@/components/Help'
import Feedback from '@/components/Feedback'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Welcome',
      component: Welcome
    },
    {
      path: '/overview',
      name: 'Overview',
      component: Overview
    },
    {
      path: '/user/:id',
      name: 'User Page',
      component: User
    },
    {
      path: '/help',
      name: 'Help',
      component: Help
    },
    {
      path: '/support',
      name: 'Support',
      component: Support
    },
    {
      path: '/feedback',
      name: 'Feedback',
      component: Feedback
    },
    {
      path: '/issues',
      beforeEnter () { location.href = 'https://gitlab.com/Yoroshi/wkclass/issues' }
    },
    {
      path: '/reviews',
      beforeEnter () { location.href = 'https://www.wanikani.com/review' }
    },
    {
      path: '/lessons',
      beforeEnter () { location.href = 'https://www.wanikani.com/lesson' }
    }
  ]
})
