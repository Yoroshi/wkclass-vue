# wkclass-pwa

> Vue.js PWA for WaniKani Class

## How to run in develop mode

1. Clone the git (create a branch)
2. Open a cli inside the public folder
3. run 'npm install' to ensure you have all the packages
4. run 'npm run dev' to start the local development server

## Inbuilt Linting

For a warning, the program has inbuilt linting to ensure a standardized coding design, the program will not let you change things without being correct to es6 standards.
